//import { Router } from 'express';
const Router = require("express");
//import botRouter from './search';
const botRouter = require("./search");

const v1Router = Router();
v1Router.use('/api/v1', botRouter);

//export default v1Router;
module.exports = v1Router;
