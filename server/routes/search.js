//import { Router } from 'express';
const Router = require("express");
//import WhatsappBot from '../controllers/WhatsappBot';
const WhatsappBot = require("../controllers/WhatsappBot");

const botRouter = Router();

botRouter.post('/incoming', WhatsappBot.googleSearch);

//export default botRouter;
module.exports = botRouter;
